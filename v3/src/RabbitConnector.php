<?php

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitConnector {
	private $connection;
	private $channel;
	private $queue;

	public function __construct($host, $port, $user, $password, $queue='') {
		$this->connection = new AMQPStreamConnection($host, $port, $user, $password);
		$this->channel = $this->connection->channel();
		$this->channel->queue_declare($queue, false, true, false, false);
		$this->queue = $queue;
	}

	public function publishMsg($data) {
		$msg = new AMQPMessage($data, array('delivery_mode' => 2));
		$this->channel->basic_publish($msg, '', $this->queue);
	}	

	public function __destruct() {
		$this->channel->close();
		$this->connection->close();
	}
}

