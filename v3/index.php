<?php

require_once __DIR__.'/vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

$rmq = new RabbitConnector('localhost', 5672, 'guest', 'guest', 'test_queue');

$app = new Silex\Application();
$app['debug'] = false;
$app['controllers']->assert('id', '\d+')->assert('channelKey', '\w+');

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
	if ($app['debug']) {
		return;
	}
	switch ($code) {
		case 404:
			$message = 'The requested page could not be found.';
			break;
		case 405:
			$message = 'The request method not allowed for this page.';
			break;
		default:
			$message = 'Something went terribly wrong.';
	}

	return $app->json($message);
});

$app->get('/', function () use ($app) {
	$subRequest = Request::create('/channel/list', 'GET');
	return $app->handle($subRequest, HttpKernelInterface::SUB_REQUEST);
});


/*
  GET - Return list of channel keys
  /channel/list
*/
$app->get('/channel/list', function () {
	$array = array(
		array(
			'channels' => array(
				array(
					'name' => 'First Channel',
					'key' => 'first_channel'
				),
				array(
					'name' => 'Second Channel',
					'key' => 'second_channel'
				),
				array(
					'name' => 'Third Channel',
					'key' => 'third_channel'
				)
			)
		)
	);

	return json_encode($array);
});

/*
  GET - Search products in channel
  /channel/{channelId}/product/search
*/
$app->get('/channel/{channelId}/product/search', function (Request $request) use ($app) {
	$channelKey = $app->escape($request->get('channelId'));
	$queryStr = $app->escape($request->get('s'));

	$msg = 'Searching product in Channel: '.$channelKey.' for: '.$queryStr;
	return $app->json($msg); 
});

/*
  PUT - Create new product
  /channel/{channel}/product/
*/
$app->put('/channel/{channelId}/product', function (Request $request) use ($app, $rmq) {
	$channelKey = $app->escape($request->get('channelId'));
	$productName = $app->escape($request->get('product_name'));

	// Lookup to see if exists

	// Insert and get Id

	$productId = 200;

	$msg = 'Creating Product in Channel: '.$channelKey.' with name='.$productName;

	// TODO Build a action structure.
	// TODO Serialize
	$data = array(
		'product' => array(
			'action' => 'add',
			'data' => array(
				'id' => $productId,
				'name' => $productName
			)
		)
	);
	$rmq->publishMsg(json_encode($data));
	return $app->json($msg); 
});

/*
  POST - Update product by id
  /channel/{channel}/product/{id}
*/
$app->post('/channel/{channelId}/product/{id}', function (Request $request) use ($app) {
	$channelKey = $app->escape($request->get('channelId'));
	$productId = $app->escape($request->get('id'));
	$productName = $app->escape($request->get('product_name'));

	// Lookup to see if exists

	// Insert

	$msg = 'Updating Product: '.$productId.' in Channel: '.$channelKey.' with name='.$productName;
	return $app->json($msg); 
});

/*
  GET - Lookup product by id
  /channel/{channel}/product/{id}
*/
$app->get('/channel/{channelId}/product/{id}', function (Request $request) use ($app) {
	$channelKey = $app->escape($request->get('channelId'));
	$productId = $app->escape($request->get('id'));

	// Lookup to see if exists

	$msg = 'Looking up Product: '.$productId.' in Channel: '.$channelKey;
	return $app->json($msg); 
});

/*
  DELETE - Remove product from DB
  /channel/{channel}/product/{id}
*/
$app->delete('/channel/{channelKey}/product/{id}', function (Request $request) use ($app, $rmq) {
	$channelKey = $request->get('channelKey');
	$productId = $request->get('id');

	// Lookup to see if exists

	// Delete

	$msg = 'Deleting Product: '.$productId.' in Channel: '.$channelKey;
	// TODO Build a action structure.
	// TODO Serialize
	$data = array(
		'product' => array(
			'action' => 'delete',
			'data' => array(
				'id' => $productId,
			)
		)
	);
	$rmq->publishMsg(json_encode($data));
	return $app->json($msg); 
});

$app->run();
