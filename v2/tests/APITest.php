<?php

require_once __DIR__.'/../vendor/autoload.php';
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Silex\WebTestCase;

class APITest extends WebTestCase {
	public $app;

	public function createApplication() {
		$this->app = require __DIR__.'/../index.php';
		unset($this->app['exception_handler']);

		return $app;
	}

	public function testBasePath() {
		$test = Request::create('/', 'GET');
		$result = $this->app->handle($test, HttpKernelInterface::SUB_REQUEST);

		$expectedResult = '[{"channels":[{"name":"First Channel","key":"first_channel"},{"name":"Second Channel","key":"second_channel"},{"name":"Third Channel","key":"third_channel"}]}]';
		$this->assertEquals($result->getContent(), $expectedResult);
	}

	public function testChannelListPath() {
		$test = Request::create('/channel/list', 'GET');
		$result = $this->app->handle($test, HttpKernelInterface::SUB_REQUEST);

		$expectedResult = '[{"channels":[{"name":"First Channel","key":"first_channel"},{"name":"Second Channel","key":"second_channel"},{"name":"Third Channel","key":"third_channel"}]}]';
		$this->assertEquals($result->getContent(), $expectedResult);
	}

	public function testSearchPath() {
		$channelKey = 'fooo';
		$queryStr = 'test';
		$query = '/channel/'.$channelKey.'/product/search?'.http_build_query(array('s' => $queryStr));
		$expectedResult = '"Searching product in Channel: '.$channelKey.' for: '.$queryStr.'"';
		$test = Request::create($query, 'GET');
		$result = $this->app->handle($test, HttpKernelInterface::SUB_REQUEST);

		$this->assertEquals($result->getContent(), $expectedResult);
	}

	public function testAddChannelProduct() {

		$channelKey = 'fooo';
		$name = 'test';
		$query = '/channel/'.$channelKey.'/product'; //?'.http_build_query(array('s' => $queryStr));
		$expectedResult = '"Creating Product in Channel: '.$channelKey.' with name='.$name.'"';

		$test = Request::create($query, 'PUT', array('product_name' => $name));
		$result = $this->app->handle($test, HttpKernelInterface::SUB_REQUEST);

		$this->assertEquals($result->getContent(), $expectedResult);
	}

	public function testUpdateChannelProduct() {
		$channelKey = 'fooo';
		$id=200;
		$name = 'test';
		$query = '/channel/'.$channelKey.'/product/'.$id;
		$expectedResult = '"Updating Product: '.$id.' in Channel: '.$channelKey.' with name='.$name.'"';
		$test = Request::create($query, 'POST', array('product_name' => $name));
		$result = $this->app->handle($test, HttpKernelInterface::SUB_REQUEST);

		$this->assertEquals($result->getContent(), $expectedResult);
	}

	public function testDeleteChannelProduct() {
		$channelKey = 'fooo';
		$id=200;
		$query = '/channel/'.$channelKey.'/product/'.$id;
		$expectedResult = '"Deleting Product: '.$id.' in Channel: '.$channelKey.'"';
		// $test = Request::create($query, 'DELETE');
		$test = Request::create($query, 'DELETE'); //, array('id' => $id));
		$result = $this->app->handle($test, HttpKernelInterface::SUB_REQUEST);

		$this->assertEquals($result->getContent(), $expectedResult);
	}
}
