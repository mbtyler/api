<?php

require_once __DIR__.'/vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

$app = new Silex\Application();
$app['debug'] = true;
$app['controllers']->assert('id', '\d+')->assert('channelKey', '\w+');

$app->get('/', function () use ($app) {
    $subRequest = Request::create('/channel/list', 'GET');
    return $app->handle($subRequest, HttpKernelInterface::SUB_REQUEST);
});

/*
  GET - Return list of channel keys
  /channel/list
*/
$app->get('/channel/list', function () {
	$array = array(
		array(
			'channels' => array(
				array(
					'name' => 'First Channel',
					'key' => 'first_channel'
				),
				array(
					'name' => 'Second Channel',
					'key' => 'second_channel'
				),
				array(
					'name' => 'Third Channel',
					'key' => 'third_channel'
				)
			)
		)
	);

	return json_encode($array);
});

/*
  GET - Search products in channel
  /channel/{channelId}/product/search
*/
$app->get('/channel/{channelId}/product/search', function (Request $request) use ($app) {
	$channelKey = $app->escape($request->get('channelId'));
	$queryStr = $app->escape($request->get('s'));

	$msg = 'Searching product in Channel: '.$channelKey.' for: '.$queryStr;
	return $app->json($msg); 
});

/*
  PUT - Create new product
  /channel/{channel}/product/
*/
$app->put('/channel/{channelId}/product', function (Request $request) use ($app) {
	$channelKey = $app->escape($request->get('channelId'));
	$productName = $app->escape($request->get('product_name'));

	// Lookup to see if exists

	// Insert

	$msg = 'Creating  Product: '.$productId.' in Channel: '.$channelKey.' with name='.$productName;
	return $app->json($msg); 
});

/*
  POST - Update product by id
  /channel/{channel}/product/{id}
*/
$app->post('/channel/{channelId}/product/{id}', function (Request $request) use ($app) {
	$channelKey = $app->escape($request->get('channelId'));
	$productId = $app->escape($request->get('id'));
	$productName = $app->escape($request->get('product_name'));

	// Lookup to see if exists

	// Insert

	$msg = 'Updating Product: '.$productId.' in Channel: '.$channelKey.' with name='.$productName;
	return $app->json($msg); 
});

/*
  GET - Lookup product by id
  /channel/{channel}/product/{id}
*/
$app->get('/channel/{channelId}/product/{id}', function (Request $request) use ($app) {
	$channelKey = $app->escape($request->get('channelId'));
	$productId = $app->escape($request->get('id'));

	// Lookup to see if exists

	$msg = 'Looking up Product: '.$productId.' in Channel: '.$channelKey;
	return $app->json($msg); 
});

/*
  DELETE - Remove product from DB
  /channel/{channel}/product/{id}
*/
$app->delete('/channel/{channelId}/product/{id}', function (Request $request) use ($app) {
	$channelKey = $app->escape($request->get('channelId'));
	$productId = $app->escape($request->get('id'));

	// Lookup to see if exists

	// Delete

	$msg = 'Deleteing Product: '.$productId.' in Channel: '.$channelKey;
	return $app->json($msg); 
});

$app->run();
