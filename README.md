# README #

Quick versioned API with simple API tests and RabbitMQ message delivery on sensitive api requests

### How do I get set up? ###

`composer install' in each version dir.


valid paths on all 3 versions

Root - Channel List
```
method:GET /
```
Channel List
```
method:GET /channel/list
```

Search Product
```
method:GET /channel/{channelKey}/product/search?s=test
```

Add New Product - v3 Adds event to message queue
```
method:PUT /channel/{channelKey}/product DATA['product_name']
```
Lookup Product
```
method:GET /channel/{channelKey}/product/{id} 
```
Update Product
```
method:POST /channel/{channelKey}/product/{id} DATA['product_name']
```
Delete Product - v3 Adds event to message queue
```
method:DELETE /channel/{channelKey}/product/{id}
```